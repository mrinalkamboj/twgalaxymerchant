﻿namespace GalaxyMerchantGuide.Enum
{
    /// <summary>
    /// Enumeration for Input Type Categories
    /// </summary>
    public enum InputTypeEnum
    {
        Unknown,
        Assignment,
        Derivation,
        Question
    }

    /// <summary>
    /// Enumeration for Input data Validation
    /// </summary>
    public enum InputValidatorEnum
    {
        None,
        ValidAssignmentStatement,
        ValidDerivationStatement,
        ValidQuestionStatement
    }

    /// <summary>
    /// Enumeration for Content data Validation
    /// </summary>
    public enum ContentValidatorEnum
    {
        None,
        LastLetterIsValidCurrency,
        OnlyValidRomanExist,
        OnlyValidDlvExist,
        OnlyValidIxcmRepeatExist,
        OnlyValidSubtractionPatternExist
    }

    public enum InputReaderType
    {
        None,
        FileInput,
        FileCurrency
    }
}
