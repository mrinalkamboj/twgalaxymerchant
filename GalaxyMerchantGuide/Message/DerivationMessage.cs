﻿using System;
using System.Collections.Generic;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Message
{
    /// <summary>
    /// Derivation Message Class
    /// </summary>
    public class Derivation : IInputMessage
    {
        /// <summary>
        /// Derivation Message type constructor
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="messageInfo"></param>
        /// <param name="validationProcessor"></param>
        public Derivation(string actualMessage, 
                          MessageInfoBase messageInfo, 
                          Func<string, MessageInfoBase, IInputMessage, IMessageValidator> validationProcessor)
        {
            ActualMessage = actualMessage;
            MessageInfo = messageInfo;
            ValidationProcessor = validationProcessor;
            IsValid = true;
        }

        /// <summary>
        /// Original Message
        /// </summary>
        public string ActualMessage { get; set; }

        /// <summary>
        /// Derivation Validation Processing
        /// </summary>
        public Func<string, MessageInfoBase, IInputMessage, IMessageValidator> ValidationProcessor { get; set; }

        /// <summary>
        /// Message Info Data Validator (Constructor Argunment)
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Input Validation Error Message Type
        /// </summary>
        public InputValidatorEnum ValidationErrorMessage { get; set; }

        /// <summary>
        /// Content Validation Error Message Type
        /// </summary>
        public ContentValidatorEnum ContentErrorMessage { get; set; }

        /// <summary>
        /// Check Whether Message is Valid
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Assigment Validation Check
        /// </summary>
        /// <returns></returns>
        public bool ValidationCheck()
        {
            IsValid = ValidationProcessor(ConstantData.Derivation,MessageInfo, this).IsInputValid(ActualMessage, ActualMessageList);
            return IsValid;
        }

        /// <summary>
        /// Assignment Processing
        /// </summary>
        /// <returns></returns>
        public IInputMessage Process()
        {
            IsValid = ValidationProcessor(ConstantData.Derivation, MessageInfo, this).IsContentValid(ProcessedMessage, ProcessedMessageList);

            // For now doing all the necessary processing in the DerivationValidator itself

            //// Fill the Galactic Currency Mapping Dictionary
            //if (IsValid)
            //    if (!Data.GalaticCurrencyMap.ContainsKey(ProcessedMessageList.First()))
            //        Data.GalaticCurrencyMap.Add(ProcessedMessageList.First(), ProcessedMessageList.Last());

            return this;
        }

        /// <summary>
        /// Pre Processed ActualMessageList
        /// </summary>
        public List<string> ActualMessageList => ActualMessage.ConvertToList();

        /// <summary>
        /// Pre Assigned InputTypeEnum
        /// </summary>
        public InputTypeEnum InputEnum => InputTypeEnum.Derivation;

        /// <summary>
        /// Pre Assigned Ignorable Data
        /// </summary>
        public HashSet<string> IgnoreData => ConstantData.DerivationIgnoreData;

        /// <summary>
        /// Pre Processed MessageList, which removes Ignorable Data
        /// </summary>
        public List<string> ProcessedMessageList => ActualMessage.ConvertToList(IgnoreData);


        /// <summary>
        /// Pre Processed Message converted from ProcessedMessageList
        /// </summary>
        public string ProcessedMessage => ProcessedMessageList.ConvertToMessage();
    }
}
