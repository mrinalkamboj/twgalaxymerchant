﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Message
{
    /// <summary>
    /// Assignment Message Class
    /// </summary>
    public class Assignment : IInputMessage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="messageInfo"></param>
        /// <param name="validationProcessor"></param>
        public Assignment(string actualMessage, 
                          MessageInfoBase messageInfo,
                          Func<string, MessageInfoBase, IInputMessage, IMessageValidator> validationProcessor)
        {
            ActualMessage = actualMessage;
            ValidationProcessor = validationProcessor;
            MessageInfo = messageInfo;
            IsValid = true;
        }

        /// <summary>
        /// Original Assignment Message (Constructor Argunment)
        /// </summary>
        public string ActualMessage { get; set; }

        /// <summary>
        /// Assignment Validator (Constructor Argunment)
        /// </summary>
        public Func<string, MessageInfoBase, IInputMessage, IMessageValidator> ValidationProcessor { get; set; }

        /// <summary>
        /// Message Info Data Validator (Constructor Argunment)
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Input Validation Error Message Type
        /// </summary>
        public InputValidatorEnum ValidationErrorMessage { get; set; }

        /// <summary>
        /// Content Validation Error Message Type
        /// </summary>
        public ContentValidatorEnum ContentErrorMessage { get; set; }

        /// <summary>
        /// Check Whether Message is Valid
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Assigment Validation Check
        /// </summary>
        /// <returns></returns>
        public bool ValidationCheck()
        {
            IsValid = ValidationProcessor(ConstantData.Assignment, MessageInfo, this).IsInputValid(ActualMessage, ActualMessageList);
            return IsValid;
        }

        /// <summary>
        /// Assignment Processing
        /// </summary>
        /// <returns></returns>
        public IInputMessage Process()
        {
            IsValid = ValidationProcessor(ConstantData.Assignment, MessageInfo, this).IsContentValid(ProcessedMessage, ProcessedMessageList);

            // Fill the Galactic Currency Mapping Dictionary
            if (!IsValid)
                return this;

            if (!MessageInfo.GalaticCurrencyMap.ContainsKey(ProcessedMessageList.First()))
                MessageInfo.GalaticCurrencyMap.Add(ProcessedMessageList.First(), ProcessedMessageList.Last());

            return this;
        }

        /// <summary>
        /// Pre Processed ActualMessageList
        /// </summary>
        public List<string> ActualMessageList => ActualMessage.ConvertToList();

        /// <summary>
        /// Pre Assigned InputTypeEnum
        /// </summary>
        public InputTypeEnum InputEnum => InputTypeEnum.Assignment;

        /// <summary>
        /// Pre Assigned Ignorable Data
        /// </summary>
        public HashSet<string> IgnoreData => ConstantData.AssignmentIgnoreData;

        /// <summary>
        /// Pre Processed MessageList, which removes Ignorable Data
        /// </summary>
        public List<string> ProcessedMessageList => ActualMessage.ConvertToList(IgnoreData);

        /// <summary>
        /// Pre Processed Message converted from ProcessedMessageList
        /// </summary>
        public string ProcessedMessage => ProcessedMessageList.ConvertToMessage();
    }
}
