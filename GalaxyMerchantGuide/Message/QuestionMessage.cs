﻿using System;
using System.Collections.Generic;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Message
{
    /// <summary>
    /// Question Message Class
    /// </summary>
    public class Question : IInputMessage
    {
        /// <summary>
        /// Question Message Type constructor
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="messageInfo"></param>
        /// <param name="validationProcessor"></param>
        public Question(string actualMessage,MessageInfoBase messageInfo, Func<string, MessageInfoBase, IInputMessage, IMessageValidator> validationProcessor)
        {
            ActualMessage = actualMessage;
            MessageInfo = messageInfo;
            ValidationProcessor = validationProcessor;
            IsValid = true;
            TotalTradeResult = 0.0;
        }

        /// <summary>
        /// Original Question Message
        /// </summary>
        public string ActualMessage { get; set; }

        /// <summary>
        /// Question Validation Processing
        /// </summary>
        public Func<string,MessageInfoBase, IInputMessage, IMessageValidator> ValidationProcessor { get; set; }

        /// <summary>
        /// Message Info Data Validator (Constructor Argunment)
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Question Input Validation
        /// </summary>
        public InputValidatorEnum ValidationErrorMessage { get; set; }

        /// <summary>
        /// Question Content Validation
        /// </summary>
        public ContentValidatorEnum ContentErrorMessage { get; set; }

        /// <summary>
        /// Question Validity Check
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Question specific property to store the Total Trade Result value (not part of IInput)
        /// </summary>
        public double TotalTradeResult { get; set; }

        /// <summary>
        /// Validate Question
        /// </summary>
        /// <returns></returns>
        public bool ValidationCheck()
        {
            IsValid = ValidationProcessor(ConstantData.Question, MessageInfo, this).IsInputValid(ActualMessage, ActualMessageList);
            return IsValid;
        }

        /// <summary>
        /// Process Question
        /// </summary>
        /// <returns></returns>
        public IInputMessage Process()
        {
            IsValid = ValidationProcessor(ConstantData.Question, MessageInfo, this).IsContentValid(ActualMessage, ProcessedMessageList);

            return this;
        }

        public List<string> ActualMessageList => ActualMessage.ConvertToList();

        public InputTypeEnum InputEnum => InputTypeEnum.Question;

        public HashSet<string> IgnoreData => ConstantData.QuestionIgnoreData;

        public List<string> ProcessedMessageList => ActualMessage.ConvertToList(IgnoreData);

        /// <summary>
        /// Question Relevant Message
        /// </summary>
        public string ProcessedMessage => ProcessedMessageList.ConvertToMessage();
    }
}
