﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;

namespace GalaxyMerchantGuide.Reader
{
    /// <summary>
    /// Read the File and return data as List<string></string>, with each index as a line in the file
    /// </summary>
    public class FileReader : IInputReader
    {
        public async Task<List<string>> Read(InputReaderType inputReaderType)
        {
            var folder = ConstantData.DataFolder;
            var file = (inputReaderType == InputReaderType.FileCurrency)
                ? ConstantData.CurrencyText
                : ConstantData.InputText;

            var fileName = folder + @"\" + file;
            return await fileName.ReadFile();
        }
    }

}
