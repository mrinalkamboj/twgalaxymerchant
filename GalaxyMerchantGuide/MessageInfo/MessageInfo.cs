﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GalaxyMerchantGuide.ContentInfo;
using GalaxyMerchantGuide.Enum;

namespace GalaxyMerchantGuide.MessageInfo
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MessageInfoBase
    {
        /// <summary>
        /// Roman Numeral Currecy Map (for Pre defined values with Input)
        /// </summary>
        public abstract Dictionary<string, int> CurrencyMap { get; set; }

        /// <summary>
        /// Galactic Currency to Roman Numeral mapping
        /// </summary>
        public abstract Dictionary<string, string> GalaticCurrencyMap { get; set; }

        /// <summary>
        /// Trading metals value storage map
        /// </summary>
        public abstract Dictionary<string, double> TradeMetalValueMap { get; set; }

        /// <summary>
        /// Data Input Validation
        /// </summary>
        public abstract Dictionary<InputValidatorEnum, Func<string, List<string>, bool>> InputValidationMap { get; set; }

        /// <summary>
        /// Data Content Validation
        /// </summary>
        public abstract Dictionary<ContentValidatorEnum, Func<string, List<string>, bool>> ContentValidationMap { get; set; }
    }

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    public sealed class MessageInfoData : MessageInfoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public MessageInfoData()
        {
            CurrencyMap = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            GalaticCurrencyMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            TradeMetalValueMap = new Dictionary<string, double>(StringComparer.OrdinalIgnoreCase);

            InputValidationMap = new Dictionary<InputValidatorEnum, Func<string, List<string>, bool>>
            {
                [InputValidatorEnum.ValidAssignmentStatement] = (message, list) =>
                    Regex.Match(message, @"^([A-Za-z]+) is ([I|V|X|L|C|D|M])$", RegexOptions.IgnoreCase).Success,
                [InputValidatorEnum.ValidDerivationStatement] = (message, list) =>
                    Regex.Match(message, @"(.*) is ([0-9]+) ([c|C]redits)$", RegexOptions.IgnoreCase).Success,
                [InputValidatorEnum.ValidQuestionStatement] = (message, list) =>
                {
                    // Question Format validation
                    // Check whether it begins with standard strings 'how many' or 'how much'
                    var begincheck = message.StartsWith("how many", StringComparison.OrdinalIgnoreCase) ||
                                     message.StartsWith("how much", StringComparison.OrdinalIgnoreCase);

                    // Check whether it ends with '?'
                    var endcheck = message.EndsWith("?", StringComparison.OrdinalIgnoreCase);

                    return begincheck && endcheck;
                }
            };

            ContentValidationMap = new Dictionary<ContentValidatorEnum, Func<string, List<string>, bool>>
            {
                [ContentValidatorEnum.LastLetterIsValidCurrency] = (message, list) => CurrencyMap.ContainsKey(list[2].Trim()),
                [ContentValidatorEnum.OnlyValidRomanExist] = ContentInfo.ContentInfoData.CheckOnlyValidRomanExist(this),
                [ContentValidatorEnum.OnlyValidDlvExist] = ContentInfo.ContentInfoData.CheckOnlyValidDlvPatternExist(this),
                [ContentValidatorEnum.OnlyValidIxcmRepeatExist] = ContentInfo.ContentInfoData.CheckOnlyValidIxcmRepeatExist(this),
                [ContentValidatorEnum.OnlyValidSubtractionPatternExist] = ContentInfo.ContentInfoData.CheckOnlyValidSubtractionPatternExist(this)
            };
        }

        /// <inheritdoc />
        /// <summary>
        /// Roman Numeral Currecy Map (for Pre defined values with Input)
        /// </summary>
        public override Dictionary<string, int> CurrencyMap { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Galactic Currency to Roman Numeral mapping
        /// </summary>
        public override Dictionary<string, string> GalaticCurrencyMap { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Trading metals storage map
        /// </summary>
        public override Dictionary<string, double> TradeMetalValueMap { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Data Input Validation
        /// </summary>
        public override Dictionary<InputValidatorEnum, Func<string, List<string>, bool>> InputValidationMap { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Data Content Validation
        /// </summary>
        public override Dictionary<ContentValidatorEnum, Func<string, List<string>, bool>> ContentValidationMap { get; set; }
    }
}

////            // ***************************************
////            // ToDo :: Regex patterns need review (Not Working)
////            // ***************************************

////            ////var validPattern1 = @"^how many [C|c]redits is .*?$";
////            ////var validPattern2 = @"^how much .*?$";
////            ////var validPattern3 = @"^how much is .*?$";
////            ////return Regex.Match(message, validPattern1, RegexOptions.IgnoreCase).Success ||
////            ////       Regex.Match(message, validPattern2, RegexOptions.IgnoreCase).Success ||
////            ////       Regex.Match(message, validPattern3, RegexOptions.IgnoreCase).Success;
////        }
////    };