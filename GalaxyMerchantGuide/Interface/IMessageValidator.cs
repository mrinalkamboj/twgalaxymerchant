﻿using System.Collections.Generic;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Interface
{
    /// <summary>
    /// Base Interface for all validators
    /// </summary>
    public interface IMessageValidator
    {
        /// <summary>
        /// Message Info Data Validator 
        /// </summary>
        MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Aggregated Input Type
        /// </summary>
        IInputMessage InputType { get; }

        /// <summary>
        /// Input Data validation
        /// </summary>
        List<InputValidatorEnum> InputValidators { get; }

        /// <summary>
        /// Content Validation
        /// </summary>
        List<ContentValidatorEnum> ContentValidators { get; }

        /// <summary>
        /// Check Whether Input is Valid
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="actualMessageList"></param>
        /// <returns></returns>
        bool IsInputValid(string actualMessage,List<string> actualMessageList);

        /// <summary>
        /// Check Whether Content is Valid
        /// </summary>
        /// <param name="processedMessage"></param>
        /// <param name="processedMessageList"></param>
        /// <returns></returns>
        bool IsContentValid(string processedMessage, List<string> processedMessageList);
    }

}
