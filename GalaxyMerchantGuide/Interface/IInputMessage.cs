﻿using System;
using System.Collections.Generic;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Interface
{
    /// <summary>
    /// Base interface for all inputs
    /// </summary>
    public interface IInputMessage
    {
            /// <summary>
            /// Actual message for processing
            /// </summary>
            string ActualMessage { get; set; }

            /// <summary>
            /// Message Info Data Validator 
            /// </summary>
            MessageInfoBase MessageInfo { get; set; }

            /// <summary>
            /// Input Type Validator Aggregated
            /// </summary>
            Func<string,MessageInfoBase, IInputMessage, IMessageValidator> ValidationProcessor { get; set; }

            /// <summary>
            /// Input Error Message
            /// </summary>
            InputValidatorEnum ValidationErrorMessage { get; set; }

            /// <summary>
            /// Content Error Message
            /// </summary>
            ContentValidatorEnum ContentErrorMessage { get; set; }

            /// <summary>
            /// Post Validation to know whether message is valid
            /// </summary>
            bool IsValid { get; set; }

            /// <summary>
            /// Message ValidationCheck method
            /// </summary>
            /// <returns></returns>
            bool ValidationCheck();

            /// <summary>
            ///  Process the message post Validation
            /// </summary>
            /// <returns></returns>
            IInputMessage Process();

            /// <summary>
            /// List from Actual Message
            /// </summary>
            List<string> ActualMessageList { get; }

            /// <summary>
            /// InputType Enumeration, to differentiate object
            /// </summary>
            InputTypeEnum InputEnum { get; }

            /// <summary>
            /// Data Ignore collection to fetch the relevant portion for trade calculation
            /// </summary>
            HashSet<string> IgnoreData { get; }

            /// <summary>
            /// List from Processed Message
            /// </summary>
            List<string> ProcessedMessageList { get; }

            /// <summary>
            /// Data processed post removal of Ignored Data
            /// </summary>
            string ProcessedMessage { get; }  
    }
}
