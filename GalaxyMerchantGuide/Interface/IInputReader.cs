﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaxyMerchantGuide.Enum;

namespace GalaxyMerchantGuide.Interface
{
    /// <summary>
    /// Base interface for all readers
    /// </summary>
    public interface IInputReader
    {
        /// <summary>
        /// Read the Data based on Reader Type
        /// </summary>
        /// <param name="inputReaderType"></param>
        /// <returns></returns>
        Task<List<string>> Read(InputReaderType inputReaderType);
    }

}
