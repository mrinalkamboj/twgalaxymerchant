﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GalaxyMerchantGuide.Interface
{
    /// <summary>
    /// Base interface for all writers
    /// </summary>
    public interface IOutputWriter
    {
        /// <summary>
        /// Write IInput List data using injected Writer
        /// </summary>
        /// <param name="inputMessageList"></param>
        void Write(List<IInputMessage> inputMessageList);
    }

}
