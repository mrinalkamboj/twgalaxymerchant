﻿using System.Collections.Generic;
using System.Linq;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.MessageValidator
{
    /// <summary>
    /// Question message type validation
    /// </summary>
    public class QuestionMessageValidator : IMessageValidator
    {
        public QuestionMessageValidator(MessageInfoBase messageInfo, IInputMessage inputType)
        {
            MessageInfo = messageInfo;
            InputType = inputType;
        }

        /// <summary>
        /// Message Info Data Validator 
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Aggregated IInput object for Data processing
        /// </summary>
        public IInputMessage InputType { get; }

        /// <summary>
        /// Input Validation Enumeration
        /// </summary>
        public List<InputValidatorEnum> InputValidators => new List<InputValidatorEnum>
        {
            InputValidatorEnum.ValidQuestionStatement
        };

        /// <summary>
        /// Content Validation Enumeration
        /// </summary>
        public List<ContentValidatorEnum> ContentValidators => new List<ContentValidatorEnum>
        {
            ContentValidatorEnum.OnlyValidRomanExist,
            ContentValidatorEnum.OnlyValidDlvExist,
            ContentValidatorEnum.OnlyValidIxcmRepeatExist,
            ContentValidatorEnum.OnlyValidSubtractionPatternExist
        };

        /// <summary>
        /// Verify whether Question Input is Valid
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="actualMessageList"></param>
        /// <returns></returns>
        public bool IsInputValid(string actualMessage, List<string> actualMessageList)
        {
            foreach (var validator in InputValidators)
            {
                var result = MessageInfo.InputValidationMap[validator](actualMessage, actualMessageList);

                if (result)
                    continue;

                // Failed Validation Condition
                InputType.ValidationErrorMessage = validator;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verify whether Question Content is Valid
        /// </summary>
        /// <param name="processedMessage"></param>
        /// <param name="processedMessageList"></param>
        /// <returns></returns>
        public bool IsContentValid(string processedMessage, List<string> processedMessageList)
        {
            // Fetching the trading dictionary for every metal
            var tradeEquationDictionary = processedMessageList.ProcessQuestion(MessageInfo);

            // Empty Trading Dictionary means no metal, only roman numerals for transaction processing
            if (!tradeEquationDictionary.Any())
            {
                // Validate Content
                foreach (var validator in ContentValidators)
                {
                    var result =
                        MessageInfo.ContentValidationMap[validator](processedMessage, processedMessageList);

                    if (result)
                        continue;

                    // Failed Validation Condition
                    InputType.ContentErrorMessage = validator;
                    return false;
                }

                // Calculate and Add Roman Numeral value as the total trade value
                var romanPartCreditValue =
                    processedMessageList.FetchRomanValue(MessageInfo); // Fetch the Value of the Roman Numerals Equation

                if(this.InputType is Question currentQuestion)
                    currentQuestion.TotalTradeResult += romanPartCreditValue;
            }
            else
            {
                // Assumption is there can be multiple different metals in the equation
                foreach (var tradeData in tradeEquationDictionary)
                {
                    // Validate Content
                    foreach (var validator in ContentValidators)
                    {
                        var result =
                            MessageInfo.ContentValidationMap[validator](tradeData.Value.ConvertToMessage(), tradeData.Value);

                        if (result)
                            continue;

                        // Failed Content Validation Condition
                        InputType.ContentErrorMessage = validator;
                        return false;
                    }

                    // Typecast to the Current Question Object
                    var currentQuestion = (Question) this.InputType;

                    // Assign the full metal credit value in the absence of the Roman numerals
                    // Update Total Trade Result
                    if (!tradeData.Value.Any())
                            currentQuestion.TotalTradeResult += MessageInfo.TradeMetalValueMap[tradeData.Key];
                    else
                    {
                        var romanPartCreditValue = tradeData.Value.FetchRomanValue(MessageInfo); // Fetch the Value of the Roman Numerals Equation

                        var metalCreditValue = MessageInfo.TradeMetalValueMap[tradeData.Key]; // Fetch the trading metal value

                        currentQuestion.TotalTradeResult += romanPartCreditValue * metalCreditValue;
                    }
                }
            }

            return true;
        }
    }
}
