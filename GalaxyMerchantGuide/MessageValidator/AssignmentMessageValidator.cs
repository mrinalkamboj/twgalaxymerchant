﻿using System.Collections.Generic;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.MessageValidator
{
    /// <summary>
    /// Validator for Assignment Type
    /// </summary>
    public class AssignmentMessageValidator : IMessageValidator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageInfo"></param>
        /// <param name="inputType"></param>
        public AssignmentMessageValidator(MessageInfoBase messageInfo,IInputMessage inputType)
        {
            MessageInfo = messageInfo;
            InputType = inputType;
        }

        /// <summary>
        /// Message Info Data Validator (Constructor Argunment)
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Aggregated IInput Type for message processing
        /// </summary>
        public IInputMessage InputType { get; }

        /// <summary>
        /// Assignment Input Validation
        /// </summary>
        public List<InputValidatorEnum> InputValidators => new List<InputValidatorEnum>
        {
            InputValidatorEnum.ValidAssignmentStatement
        };

        /// <summary>
        /// Assignment Content Input Validation
        /// </summary>
        public List<ContentValidatorEnum> ContentValidators => new List<ContentValidatorEnum>();

        /// <summary>
        /// Check the Validity of the Assignment Input
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="actualMessageList"></param>
        /// <returns></returns>
        public bool IsInputValid(string actualMessage, List<string> actualMessageList)
        {
            
            foreach (var validator in InputValidators)
            {
                var isInputValid = MessageInfo.InputValidationMap[validator](actualMessage,actualMessageList);

                if (isInputValid)
                    continue;

                // Failed Validation Condition
                InputType.ValidationErrorMessage = validator;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check the Validity of the Assignment Content
        /// </summary>
        /// <param name="processedMessage"></param>
        /// <param name="processedMessageList"></param>
        /// <returns></returns>
        public bool IsContentValid(string processedMessage, List<string> processedMessageList)
        {
            foreach (var validator in ContentValidators)
            {
                var isContentInvalid = MessageInfo.ContentValidationMap[validator](processedMessage, processedMessageList);

                if (isContentInvalid)
                {
                    // Failed Validation Condition
                    InputType.ContentErrorMessage = validator;
                    return false;
                }
            }

            return true;
        }
    }

    
}
