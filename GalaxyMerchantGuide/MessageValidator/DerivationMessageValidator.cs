﻿using System.Collections.Generic;
using System.Linq;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.MessageValidator
{
    /// <summary>
    /// Validator for Derivation Type
    /// </summary>
    public class DerivationMessageValidator : IMessageValidator
    {
        public DerivationMessageValidator(MessageInfoBase messageInfo, IInputMessage inputType)
        {
            MessageInfo = messageInfo;
            InputType = inputType;
        }

        /// <summary>
        /// Message Info Data Validator 
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Aggregated IInput Type for message processing
        /// </summary>
        public IInputMessage InputType { get; }

        /// <summary>
        /// Assignment Input Validation
        /// </summary>
        public List<InputValidatorEnum> InputValidators => new List<InputValidatorEnum>
        {
            InputValidatorEnum.ValidDerivationStatement
        };

        /// <summary>
        /// Derivation Content Input Validation
        /// </summary>
        public List<ContentValidatorEnum> ContentValidators => new List<ContentValidatorEnum>
        {
            ContentValidatorEnum.OnlyValidRomanExist,
            ContentValidatorEnum.OnlyValidDlvExist,
            ContentValidatorEnum.OnlyValidIxcmRepeatExist,
            ContentValidatorEnum.OnlyValidSubtractionPatternExist
        };

        /// <summary>
        /// Check the Validity of the Derivation Input
        /// </summary>
        /// <param name="actualMessage"></param>
        /// <param name="actualMessageList"></param>
        /// <returns></returns>
        public bool IsInputValid(string actualMessage, List<string> actualMessageList)
        {
            foreach (var validator in InputValidators)
            {
                var isInputValid = MessageInfo.InputValidationMap[validator](actualMessage, actualMessageList);

                if (isInputValid)
                    continue;

                // Failed Validation Condition
                InputType.ValidationErrorMessage = validator;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verify whether Derivation Content is Valid
        /// </summary>
        /// <param name="processedMessage"></param>
        /// <param name="processedMessageList"></param>
        /// <returns></returns>
        public bool IsContentValid(string processedMessage, List<string> processedMessageList)
        {
            // Fetching the Roman Numeral Part, preceding the Trade Metal
            var finalCreditValue = double.Parse(processedMessageList.Last());

            // Fetching the trading dictionary for every metal
            var tradeDictionary = processedMessageList.ProcessDerivation(MessageInfo);

            foreach (var tradeData in tradeDictionary)
            {
                foreach (var validator in ContentValidators)
                {
                    var isContentValid = MessageInfo.ContentValidationMap[validator](tradeData.Value.ConvertToMessage(), tradeData.Value);

                    if (!isContentValid)
                    {
                        // Failed Validation Condition
                        InputType.ContentErrorMessage = validator;
                        return false;
                    }
                }

                // Assign the full credit value in the absence of the Roman numerals
                if(!tradeData.Value.Any())
                    MessageInfo.TradeMetalValueMap.Add(tradeData.Key,finalCreditValue);
                else
                {
                    // Divide by RomanNumeral credit to find metal value
                    var romanPartCreditValue = tradeData.Value.FetchRomanValue(MessageInfo);
                    var metalCreditValue = (finalCreditValue / romanPartCreditValue);
                    MessageInfo.TradeMetalValueMap.Add(tradeData.Key, metalCreditValue);
                }
            }

            return true;
        }
    }
}
