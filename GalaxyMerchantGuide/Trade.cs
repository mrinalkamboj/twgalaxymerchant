﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide
{
    /////// <summary>
    /////// Trade class to process all the Trading requirements
    /////// </summary>
    ////public class Trade
    ////{
    ////    /// <summary>
    ////    /// Constructor with injection for Reader, InputProcessor and Writer
    ////    /// </summary>
    ////    /// <param name="reader"></param>
    ////    /// <param name="inputProcessor"></param>
    ////    /// <param name="writer"></param>
    ////    public Trade(IInputReader reader, Func<string, string, IInputMessage> inputProcessor,IOutputWriter writer)
    ////    {
    ////        Reader = reader;
    ////        InputProcessor = inputProcessor;
    ////        Writer = writer;
    ////        InputDataList = new List<string>();
    ////        InputMessageList = new List<IInputMessage>();
    ////    }

    ////    /// <summary>
    ////    /// Read Input
    ////    /// </summary>
    ////    public IInputReader Reader { get; set; }

    ////    /// <summary>
    ////    /// Write Output
    ////    /// </summary>
    ////    public IOutputWriter Writer { get; set; }

    ////    /// <summary>
    ////    /// Process input messages
    ////    /// </summary>
    ////    public Func<string, string, IInputMessage> InputProcessor { get; set; }

    ////    /// <summary>
    ////    /// Input Data
    ////    /// </summary>
    ////    public List<string> InputDataList { get; set; }

    ////    /// <summary>
    ////    ///  Processed Message Data
    ////    /// </summary>
    ////    public List<IInputMessage> InputMessageList { get; set; }

    ////    /// <summary>
    ////    /// Read the Input and store it in memory
    ////    /// </summary>
    ////    /// <returns></returns>
    ////    public async Task ReadInput()
    ////    {
    ////        var currencyTextDataList = await Reader.Read(InputReaderType.FileCurrency);

    ////        currencyTextDataList.FillCurrencyMap();

    ////        InputDataList = await Reader.Read(InputReaderType.FileInput);

    ////        InputMessageList = new List<IInputMessage>();
    ////    }

    ////    /// <summary>
    ////    /// Begin message processing
    ////    /// </summary>
    ////    public void BeginProcessing()
    ////    {
    ////        // Process every message
    ////        foreach (var data in InputDataList)
    ////        {
    ////            var assignmentObj = InputProcessor(ConstantData.Assignment, data);
    ////            var derivationObj = InputProcessor(ConstantData.Derivation, data);
    ////            var questionObj = InputProcessor(ConstantData.Question, data);

    ////            if (assignmentObj.ValidationCheck()) // Assignment Data Processing
    ////            {
    ////                var inputMessage = assignmentObj.Process();
    ////                InputMessageList.Add(inputMessage);
    ////            }
    ////            else if (derivationObj.ValidationCheck()) // Derivation Data Processing
    ////            {
    ////                var inputMessage = derivationObj.Process();
    ////                InputMessageList.Add(inputMessage);
    ////            }
    ////            else if (questionObj.ValidationCheck()) // Question Data Processing
    ////            {
    ////                var inputMessage = questionObj.Process();
    ////                InputMessageList.Add(inputMessage);
    ////            }
    ////        }
    ////    }

    ////    /// <summary>
    ////    /// Write Output
    ////    /// </summary>
    ////    public void WriteOutput()
    ////    {
    ////        Writer.Write(InputMessageList);
    ////    }
    ////}

    /// <summary>
    /// Trade class to process all the Trading requirements
    /// </summary>
    public class Trade
    {
        /// <summary>
        /// Constructor with injection for Reader, InputProcessor and Writer
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="messageInfo"></param>
        /// <param name="inputProcessor"></param>
        /// <param name="writer"></param>
        public Trade(IInputReader reader, 
                       MessageInfoBase messageInfo, 
                       Func<string, string, MessageInfoBase,IInputMessage> inputProcessor, 
                       IOutputWriter writer)
        {
            Reader = reader;
            MessageInfo = messageInfo;
            InputProcessor = inputProcessor;
            Writer = writer;
            InputDataList = new List<string>();
            InputMessageList = new List<IInputMessage>();
        }

        /// <summary>
        /// Read Input
        /// </summary>
        public IInputReader Reader { get; set; }

        /// <summary>
        /// Write Output
        /// </summary>
        public IOutputWriter Writer { get; set; }

        /// <summary>
        /// Process input messages
        /// </summary>
        public Func<string, string, MessageInfoBase, IInputMessage> InputProcessor { get; set; }

        /// <summary>
        /// Message Data Validation
        /// </summary>
        public MessageInfoBase MessageInfo { get; set; }

        /// <summary>
        /// Input Data
        /// </summary>
        public List<string> InputDataList { get; set; }

        /// <summary>
        ///  Processed Message Data
        /// </summary>
        public List<IInputMessage> InputMessageList { get; set; }

        /// <summary>
        /// Read the Input and store it in memory
        /// </summary>
        /// <returns></returns>
        public async Task ReadInput()
        {
            // Read the Currency File
            var currencyTextDataList = await Reader.Read(InputReaderType.FileCurrency);

            // Fill MessageData Validation objects
            currencyTextDataList.FillCurrencyMap(MessageInfo);

            // Read the Input File
            InputDataList = await Reader.Read(InputReaderType.FileInput);

            // Persist details of different Message Types
            InputMessageList = new List<IInputMessage>();
        }

        /// <summary>
        /// Begin message processing
        /// </summary>
        public void BeginProcessing()
        {
            // Process every message
            foreach (var data in InputDataList)
            {
                var assignmentObj = InputProcessor(ConstantData.Assignment, data,MessageInfo);
                var derivationObj = InputProcessor(ConstantData.Derivation, data, MessageInfo);
                var questionObj = InputProcessor(ConstantData.Question, data, MessageInfo);

                if (assignmentObj.ValidationCheck()) // Assignment Data Processing
                {
                    var inputMessage = assignmentObj.Process();
                    InputMessageList.Add(inputMessage);
                }
                else if (derivationObj.ValidationCheck()) // Derivation Data Processing
                {
                    var inputMessage = derivationObj.Process();
                    InputMessageList.Add(inputMessage);
                }
                else if (questionObj.ValidationCheck()) // Question Data Processing
                {
                    var inputMessage = questionObj.Process();
                    InputMessageList.Add(inputMessage);
                }
            }
        }

        /// <summary>
        /// Write Output
        /// </summary>
        public void WriteOutput()
        {
            Writer.Write(InputMessageList);
        }
    }

}
