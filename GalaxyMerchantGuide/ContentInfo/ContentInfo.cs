﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.ContentInfo
{
    public abstract class ContentInfoBase
    {
        public abstract MessageInfoBase MessageInfo { get; set; }

        // List of Roman Numerals, which cannot be Repeated
        public abstract List<string> NonRepeatRomans { get; set; }

        // List of Roman numerals, which can be repeated with few valid conditions
        public abstract List<string> RepeatRomans { get; set; }

        public abstract Func<string, List<string>, bool> CheckOnlyValidRomanExist();

        public abstract Func<string, List<string>, bool> CheckOnlyValidDlvPatternExist();

        public abstract Func<string, List<string>, bool> CheckOnlyValidIxcmRepeatExist();

        public abstract Func<string, List<string>, bool> CheckOnlyValidSubtractionPatternExist();

        public abstract string FetchInValidRepeatPatterns();

        public abstract string FetchInValidSubtractionPatterns();
    }

    /// <summary>
    /// Valdation methods for Roman Numerals Data
    /// </summary>
    public static class ContentInfoData
    {
    // List of Roman Numerals, which cannot be Repeated
    public static List<string> NonRepeatRomans = new List<string> { "D", "L", "V" };

    // List of Roman numerals, which can be repeated with few valid conditions
    public static List<string> RepeatRomans = new List<string> { "I", "X", "C", "M" };

    /// <summary>
    /// Check for Valid Roman numerals, with pre defined values
    /// </summary>
    /// <returns></returns>
    public static Func<string, List<string>, bool> CheckOnlyValidRomanExist(MessageInfoBase messageInfo)
    {
        // Conditions InputList Shall not be empty
        // Input Currency Data shall exist either in the standard or galactic currency map
        return (message, inputList) =>
        {
            if (!inputList.Any())
                return false;

            if (inputList.Any(roman => !messageInfo.CurrencyMap.ContainsKey(roman) &&
                                       !messageInfo.GalaticCurrencyMap.ContainsKey(roman)))
                return false;

            return true;
        };
    }

    /// <summary>
    /// Check DLV repeat Pattern
    /// </summary>
    /// <returns></returns>
    public static Func<string, List<string>, bool> CheckOnlyValidDlvPatternExist(MessageInfoBase messageInfo)
    {
        return (message, inputList) =>
        {
            var romanLookUp = inputList.ToLookup(roman => messageInfo.GalaticCurrencyMap.ContainsKey(roman) ?
                                                          messageInfo.GalaticCurrencyMap[roman] :
                                                          roman, StringComparer.OrdinalIgnoreCase);

            foreach (var data in NonRepeatRomans)
            {
                if (romanLookUp.Contains(data) &&
                    romanLookUp[data].Count() > 1)
                    return false;
            }

            return true;
        };
    }

    /// <summary>
    /// Check IXCM repeat pattern, Whetherits valid
    /// </summary>
    /// <returns></returns>
    public static Func<string, List<string>, bool> CheckOnlyValidIxcmRepeatExist(MessageInfoBase messageInfo)
    {
        return (message, inputList) =>
        {
            var integratedInput = string.Empty;

            foreach (var inputValue in inputList)
                integratedInput += messageInfo.GalaticCurrencyMap.ContainsKey(inputValue) ?
                                   messageInfo.GalaticCurrencyMap[inputValue] :
                                   inputValue;

            //integratedInput = inputList.Skip(1).Aggregate(inputList.First(), (x, y) => $"{Data.GalaticCurrencyMap.ContainsKey(x)} ? {Data.GalaticCurrencyMap[x]} : {x}" +
            //                                                                               $"{Data.GalaticCurrencyMap.ContainsKey(y)} ? {Data.GalaticCurrencyMap[y]} : {y}");

            var invalidPatterns = FetchInValidRepeatPatterns(messageInfo);

            var returnValue = !Regex.Match(integratedInput, invalidPatterns, RegexOptions.IgnoreCase).Success;

            return returnValue;
        };
    }

    /// <summary>
    /// Verify the Valid Pattern to Subtract Roman Numerals
    /// </summary>
    /// <returns></returns>
    public static Func<string, List<string>, bool> CheckOnlyValidSubtractionPatternExist(MessageInfoBase messageInfo)
    {
        return (message, inputList) =>
        {
            var integratedInput = string.Empty;

            foreach (var inputValue in inputList)
                integratedInput += messageInfo.GalaticCurrencyMap.ContainsKey(inputValue) ?
                    messageInfo.GalaticCurrencyMap[inputValue] :
                    inputValue;

            var invalidPatterns = FetchInValidSubtractionPatterns();

            var returnValue = !Regex.Match(integratedInput, invalidPatterns, RegexOptions.IgnoreCase).Success;

            return returnValue;
        };
    }

    /// <summary>
    /// Roman Numerals Incorrect Repetition Patterns
    /// </summary>
    /// <returns></returns>
    public static string FetchInValidRepeatPatterns(MessageInfoBase messageInfo)
    {
        var returnList = new List<string>();

        foreach (var r in RepeatRomans)
        {
            returnList.Add($"{r}{r}{r}{r}");

            var higherValueList = messageInfo.CurrencyMap.Where(x => x.Value > messageInfo.CurrencyMap[r]).Select(x => x.Key).ToList();

            foreach (var h in higherValueList)
                returnList.Add($"{r}{r}{r}{h}{r}");
        }

        return "(" + returnList.Skip(1).Aggregate(returnList.First(), (x, y) => $"{x}|{y}") + ")+";
    }

    /// <summary>
    /// Roman Numeral Value Caluclation, these Patterns are Disallowed
    /// </summary>
    /// <returns></returns>
    public static string FetchInValidSubtractionPatterns()
    {
        // These are Invalid Subtraction Patterns
        // Only certain higher values can follow the lower values

        return "(IL|IC|ID|IM|XD|XM)+";
    }
}
}
