﻿using System;
using System.Collections.Generic;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;

namespace GalaxyMerchantGuide.Writer
{
    /// <summary>
    /// IWriter Type which writes to the Console
    /// </summary>
    public class ConsoleWriter : IOutputWriter
    {
        /// <summary>
        /// Traverse through Input Message List and print the relevant data
        /// </summary>
        /// <param name="inputMessageList"></param>
        public void Write(List<IInputMessage> inputMessageList)
        {
            foreach (var input in inputMessageList)
            {
                if (!input.InputEnum.Equals(InputTypeEnum.Question))
                    continue;

                var question = (Question)input;

                if (question.IsValid)
                    Console.WriteLine("Question :: " + question.ActualMessage + "; Value :: " + question.TotalTradeResult);
                else
                    Console.WriteLine("Question :: " + question.ActualMessage + "; Value :: CANNOT DECIPHER !!");
            }
        }
    }
}
