﻿using System;
using System.Collections.Generic;

namespace GalaxyMerchantGuide.Constants
{
    /// <summary>
    /// Constant Data for the Project.
    /// </summary>
    public static class ConstantData
    {
        // Constant Value

        // Files and Folders
        public static string DataFolder = $"{Environment.CurrentDirectory}\\..\\..\\Input";
        public const string InputText = @"Input.txt";
        public const string CurrencyText = @"Currency.txt";

        // DI binding constructor constants
        public const string ConstructorArgActualMessage = "actualMessage";
        public const string ConstructorArgMessageInfoData = "messageInfo";
        public const string ConstructorArgValidationProcessor = "validationProcessor";
        public const string ConstructorArgInputType = "inputType";

        // Input Types - Assignment, Derivation and Question
        public const string Assignment = "Assignment";
        public const string Derivation = "Derivation";
        public const string Question = "Question";

        // Assignment Type Constants
        public static readonly HashSet<string> AssignmentIgnoreData = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "is" };

        // Derivation Type Constants
        public static readonly HashSet<string> DerivationIgnoreData = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "is", "Credits" };

        // Question Type Constants
        public static readonly HashSet<string> QuestionIgnoreData = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "is", "Credits", "how", "much", "many", "?" }; 
    }
}
