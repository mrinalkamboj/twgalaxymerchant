﻿using System;
using System.Threading.Tasks;
using GalaxyMerchantGuide.DependencyInjection;
using Ninject;

namespace GalaxyMerchantGuide
{

    /// <summary>
    /// Main Entry class to Begin Trading
    /// </summary>
    public class Merchant
    {
        /// <summary>
        /// Main Entry Method
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            // Injects and process using Trade Class
            var trade = NinjectHelper.FetchKernel().Value.Get<Trade>();

            await trade.ReadInput(); // Read Input

            trade.BeginProcessing(); // Process Input

            trade.WriteOutput(); // Write Output
        }
    }
}
