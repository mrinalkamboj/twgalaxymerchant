﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuide.Extension
{
    /// <summary>
    /// File Extension methods
    /// </summary>
    public static class FileExtensions
    {
        // Async Read File and return a List<string> containing every line as an element
        public static async Task<List<string>> ReadFile(this string textFilePath)
        {
            // File Retuen Data List
            var fileData = new List<string>();

            // Create FileStream
            var filestream = new FileStream(textFilePath, FileMode.Open, FileAccess.Read);

            // Create StreamReader
            var file = new StreamReader(filestream, Encoding.UTF8, true, 128);

            // Every line of text read from the File
            string lineOfText = null;

            // Loop and Add Data
            while ((lineOfText = await file.ReadLineAsync()) != null)
                fileData.Add(lineOfText);

            return fileData;
        }
    }

    /// <summary>
    /// Extension methods for List<string></string> type 
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Extension method to fill the MessageBase Data Dictionary
        /// </summary>
        /// <param name="currencyMapData"></param>
        /// <param name="messageInfo"></param>
        public static void FillCurrencyMap(this List<string> currencyMapData,MessageInfoBase messageInfo)
        {
            foreach (var currency in currencyMapData)
            {
                // Trim the whitespaces from beginning and the end
                var currencyKeyValueArray = currency.ConvertToList();

                // Strict Condition, consider Only the arrays with 2 elements
                if (currencyKeyValueArray.Count != 2)
                    continue;

                // Fill CurrencyMap Dictionary
                if (!messageInfo.CurrencyMap.ContainsKey(currencyKeyValueArray.First()))
                    messageInfo.CurrencyMap.Add(currencyKeyValueArray.First(), Convert.ToInt32(currencyKeyValueArray.Last())); // Add new KV pair
                else
                    messageInfo.CurrencyMap[currencyKeyValueArray.First()] = Convert.ToInt32(currencyKeyValueArray.Last()); // Update the existing KV
            }
        }

        public static string ConvertToMessage(this List<string> processedMessage)
        {
            if (!processedMessage.Any())
                return string.Empty;
            return processedMessage.Skip(1).Aggregate(processedMessage.First(), (one, two) => one + " " + two);
        }

        /// <summary>
        ///  Process the Derivation Validation Request
        /// </summary>
        /// <param name="processedMessageList"></param>
        /// <param name="messageInfo"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> ProcessDerivation(this List<string> processedMessageList, MessageInfoBase messageInfo)
        {
            ////// Fetch the Portion of ProcessMessageList, which excludes the Credit Value
            ////var processedMessageListWithoutCredit = processedMessageList.GetRange(0, processedMessageList.Count - 1);

            // Fetch TradeMetal List, which are used for trading and is an exchange commodity, like Silver, Gold Iron
            var tradeMetalsList = processedMessageList.Where(currency => !messageInfo.CurrencyMap.ContainsKey(currency) &&
                                                                         !messageInfo.GalaticCurrencyMap.ContainsKey(currency)).ToList();

            // Data strcuture to hold the Trade Data Equation Information
            var tradeMetalsEquationDictionaryList = new Dictionary<string, List<string>>();

            if (!tradeMetalsList.Any())
                return tradeMetalsEquationDictionaryList;

            // Traversal Index to avoid the data repetition
            var traversalIndex = 0;

            // Traverse via Trade Metal List and process based on trade metals
            foreach (var tradeMetal in tradeMetalsList.GetRange(0, tradeMetalsList.Count-1)) // Ignore the Trade Value in the end
            {
                var traversalList =
                    processedMessageList.GetRange(traversalIndex, processedMessageList.Count-1 - traversalIndex);

                var localEquationList = new List<string>();

                foreach (var currencyData in traversalList)
                {
                    if (currencyData.Equals(tradeMetal))
                    {
                        traversalIndex++;
                        break;
                    }
                    // Fetch the Curency Data either from Galactic Currency Map or Generic Currency Map
                    localEquationList.Add(messageInfo.GalaticCurrencyMap.ContainsKey(currencyData) ? messageInfo.GalaticCurrencyMap[currencyData] : currencyData);
                    traversalIndex++;
                }

                if (!tradeMetalsEquationDictionaryList.ContainsKey(tradeMetal))
                    tradeMetalsEquationDictionaryList.Add(tradeMetal, localEquationList);
                else
                    tradeMetalsEquationDictionaryList[tradeMetal] = localEquationList;
            }

            return tradeMetalsEquationDictionaryList;
        }

        /// <summary>
        /// Process the Question Validation request
        /// </summary>
        /// <param name="processedMessageList"></param>
        /// <param name="messageInfo"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> ProcessQuestion(this List<string> processedMessageList, MessageInfoBase messageInfo)
        {
            // Fetch TradeMetal List, which are used for trading and is an exchange commodity, like "Silver,Gold,Iron"
            var tradeMetalsList = processedMessageList.Where(currency => !messageInfo.CurrencyMap.ContainsKey(currency) &&
                                                                         !messageInfo.GalaticCurrencyMap.ContainsKey(currency)).ToList();

            // Data strcuture to hold the Trade Data Equation Information, Key is the metal and value is Roman numeral equation
            var tradeMetalsEquationDictionaryList = new Dictionary<string, List<string>>();

            // Return Empty Dictionary to Process Only Roman Numerals
            if (!tradeMetalsList.Any())
                return tradeMetalsEquationDictionaryList;
            
            // Traversal Index to avoid the data repetition
            var traversalIndex = 0;

            // Traverse via Trade Metal List and process based on trade metals
            // IMP :: Current assumption every metal appera only once, otherwise we need to modify the Datastrcuture
            foreach (var tradeMetal in tradeMetalsList)
            {
                var traversalList =
                    processedMessageList.GetRange(traversalIndex, processedMessageList.Count - 1 - traversalIndex);

                var localEquationList = new List<string>();

                foreach (var currencyData in traversalList)
                {
                    if (currencyData.Equals(tradeMetal))
                    {
                        traversalIndex++;
                        break;
                    }

                    // Fetch the Curency Data either from Galactic Currency Map or Generic Currency Map
                    localEquationList.Add(messageInfo.GalaticCurrencyMap.ContainsKey(currencyData) ? messageInfo.GalaticCurrencyMap[currencyData] : currencyData);
                    traversalIndex++;
                }

                if (!tradeMetalsEquationDictionaryList.ContainsKey(tradeMetal))
                    tradeMetalsEquationDictionaryList.Add(tradeMetal, localEquationList);
                else
                    tradeMetalsEquationDictionaryList[tradeMetal] = localEquationList; //Replace any existing Key Data, doesn't handle metal repetition
            }

            return tradeMetalsEquationDictionaryList;
        }

        /// <summary>
        /// Fetch the Roman Value for the Galactic Currency, using the processing logic as specified
        /// Subtraction from the higher value and skip (As per problem definition)
        /// </summary>
        /// <param name="tradeDataList"></param>
        /// <param name="messageInfo"></param>
        /// <returns></returns>
        public static int FetchRomanValue(this List<string> tradeDataList, MessageInfoBase messageInfo)
        {
            var skipNext = false;
            var finalRomanDataValue = 0;

            for (var traverseIndex = 0; traverseIndex < tradeDataList.Count;)
            {
                if (skipNext)
                {
                    skipNext = false;
                    traverseIndex++;
                    continue;
                }

                // Galactic Currency Data could be fetched from Data.GalaticCurrencyMap or Data.CurrencyMap
                var currentValue = messageInfo.GalaticCurrencyMap.ContainsKey(tradeDataList[traverseIndex])
                                ? messageInfo.CurrencyMap[messageInfo.GalaticCurrencyMap[tradeDataList[traverseIndex]]]
                                : messageInfo.CurrencyMap[tradeDataList[traverseIndex]];

                if ((traverseIndex + 1) <= tradeDataList.Count - 1)
                {
                    // Galactic Currency Data could be fetched from Data.GalaticCurrencyMap or Data.CurrencyMap
                    var nextValue = messageInfo.GalaticCurrencyMap.ContainsKey(tradeDataList[traverseIndex+1])
                        ? messageInfo.CurrencyMap[messageInfo.GalaticCurrencyMap[tradeDataList[traverseIndex+1]]]
                        : messageInfo.CurrencyMap[tradeDataList[traverseIndex+1]];

                    // Subtract and Skip
                    if (nextValue > currentValue)
                    {
                        finalRomanDataValue += nextValue - currentValue;
                        skipNext = true;
                    }
                    else
                        finalRomanDataValue += currentValue;
                }
                else
                    finalRomanDataValue += currentValue;

                traverseIndex++;
            }

            // Return Final Roman Numeral Value
            return finalRomanDataValue;
        }
    }


    /// <summary>
    /// Extension methods for String data type 
    /// </summary>
    public static class StringExtensions
    {
        public static List<string> ConvertToList(this string input, HashSet<string> ignoreData = null)
        {
            // Split the String and remove the entries which are part of ignore data
            // Also remove the empty strings entries
            return input.Trim().Split(new[] {" "}, StringSplitOptions.None)
                        .Where(str => (ignoreData == null || !ignoreData.Contains(str)) && !str.Equals(" "))
                        .ToList();
        }
    }
}
