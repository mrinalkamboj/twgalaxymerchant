﻿using System;
using System.Reflection;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuide.MessageValidator;
using GalaxyMerchantGuide.Reader;
using GalaxyMerchantGuide.Writer;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;

namespace GalaxyMerchantGuide.DependencyInjection
{

    /// <summary>
    /// Ninject DI framework Helper Class
    /// </summary>
    public static class NinjectHelper
    {
        /// <summary>
        /// Ninject IKernel
        /// </summary>
        private static IKernel Kernel { get; set; }

        /// <summary>
        /// Ninject Helper static constructor to initialize the Bindings
        /// </summary>
        static NinjectHelper()
        {
            Kernel = new StandardKernel();
            Kernel.Load(Assembly.GetExecutingAssembly());
        }

        /// <summary>
        /// Ninject FetchKernel method to initiate IKernel
        /// </summary>
        /// <returns></returns>
        public static Lazy<IKernel> FetchKernel() => new Lazy<IKernel>(() => Kernel);
    }

    /// <summary>
    /// DI Bindings
    /// </summary>
    public class Bindings : NinjectModule
    {
        // Ninject Load
        public override void Load()
        {
            // Data Reader
            Bind<IInputReader>().To<FileReader>(); // Standard Binding

            // Data Writer
            Bind<IOutputWriter>().To<ConsoleWriter>(); // Standard Binding

            // Mesage Data and Validation
            Bind<MessageInfoBase>().To<MessageInfoData>(); // Standard Binding

            // Func Factory Binding for all input types, Paramneters are suuplied at runtime to create an object
            Bind<Func<string, string,MessageInfoBase, IInputMessage>>().ToMethod(ctx => (name,message,messageInfo) =>
            {
                var messageArg = new ConstructorArgument(ConstantData.ConstructorArgActualMessage, message);
                var messageInfoArg = new ConstructorArgument(ConstantData.ConstructorArgMessageInfoData, messageInfo);
                var validationProcessorArg = new ConstructorArgument(ConstantData.ConstructorArgValidationProcessor, ctx.Kernel.Get<Func<string, MessageInfoBase,IInputMessage,IMessageValidator>>());
                return ctx.Kernel.Get<IInputMessage>(name, messageArg, messageInfoArg, validationProcessorArg);
            });

            // Message Binding
            Bind<IInputMessage>().To<Assignment>().Named(ConstantData.Assignment);
            Bind<IInputMessage>().To<Derivation>().Named(ConstantData.Derivation);
            Bind<IInputMessage>().To<Question>().Named(ConstantData.Question);

            // Func Validator Binding
            Bind<Func<string, MessageInfoBase,IInputMessage, IMessageValidator>>().ToMethod(ctx => (name, messageInfo, input) =>
            {
                var inputArg = new ConstructorArgument(ConstantData.ConstructorArgInputType, input);
                var messageInfoArg = new ConstructorArgument(ConstantData.ConstructorArgMessageInfoData, messageInfo);
                return ctx.Kernel.Get<IMessageValidator>(name, messageInfoArg, inputArg);
            });

            //Validator Binding
            Bind<IMessageValidator>().To<AssignmentMessageValidator>().Named(ConstantData.Assignment);
            Bind<IMessageValidator>().To<DerivationMessageValidator>().Named(ConstantData.Derivation);
            Bind<IMessageValidator>().To<QuestionMessageValidator>().Named(ConstantData.Question);
        }
    }


}
