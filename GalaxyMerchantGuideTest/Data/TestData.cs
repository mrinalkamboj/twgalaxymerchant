﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GalaxyMerchantGuide.ContentInfo;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.MessageInfo;

namespace GalaxyMerchantGuideTest.Data
{
    public class TestMessageData
    {
        public TestMessageData(MessageInfoBase messageInfoBase)
        {
            // GalaticCurrencyMap
            GalaticCurrencyMap = new Dictionary<string, string>
            {
                ["glob"] = "I",
                ["prok"] = "V",
                ["pish"] = "X",
                ["tegj"] = "L"
            };

            // CurrencyMap
            CurrencyMap = new Dictionary<string, int>
            {
                ["I"] = 1,
                ["V"] = 5,
                ["X"] = 10,
                ["L"] = 50,
                ["C"] = 100,
                ["D"] = 500,
                ["M"] = 1000
            };

            // TradeMetalValueMap
            TradeMetalValueMap = new Dictionary<string, double>();

            // InputValidationMap
            InputValidationMap = new Dictionary<InputValidatorEnum, Func<string, List<string>, bool>>
            {
                [InputValidatorEnum.ValidAssignmentStatement] = (message, list) =>
                    Regex.Match(message, @"^([A-Za-z]+) is ([I|V|X|L|C|D|M])$", RegexOptions.IgnoreCase).Success,
                [InputValidatorEnum.ValidDerivationStatement] = (message, list) =>
                    Regex.Match(message, @"(.*) is ([0-9]+) ([c|C]redits)$", RegexOptions.IgnoreCase).Success,
                [InputValidatorEnum.ValidQuestionStatement] = (message, list) =>
                {
                    // Question Format validation
                    // Check whether it begins with standard strings 'how many' or 'how much'
                    var begincheck = message.StartsWith("how many", StringComparison.OrdinalIgnoreCase) ||
                                     message.StartsWith("how much", StringComparison.OrdinalIgnoreCase);

                    // Check whether it ends with '?'
                    var endcheck = message.EndsWith("?", StringComparison.OrdinalIgnoreCase);

                    return begincheck && endcheck;
                }
            };

            // ContentValidationMap
            ContentValidationMap = new Dictionary<ContentValidatorEnum, Func<string, List<string>, bool>>
            {
                [ContentValidatorEnum.LastLetterIsValidCurrency] = (message, list) => CurrencyMap.ContainsKey(list[2].Trim()),
                [ContentValidatorEnum.OnlyValidRomanExist] = ContentInfoData.CheckOnlyValidRomanExist(messageInfoBase),
                [ContentValidatorEnum.OnlyValidDlvExist] = ContentInfoData.CheckOnlyValidDlvPatternExist(messageInfoBase),
                [ContentValidatorEnum.OnlyValidIxcmRepeatExist] = ContentInfoData.CheckOnlyValidIxcmRepeatExist(messageInfoBase),
                [ContentValidatorEnum.OnlyValidSubtractionPatternExist] = ContentInfoData.CheckOnlyValidSubtractionPatternExist(messageInfoBase)
            };


        }

        public Dictionary<string, string> GalaticCurrencyMap { get; set; }

        public Dictionary<string, int> CurrencyMap { get; set; }

        public Dictionary<string, double> TradeMetalValueMap { get; set; }

        public Dictionary<InputValidatorEnum, Func<string, List<string>, bool>>  InputValidationMap { get; set; }

        public Dictionary<ContentValidatorEnum, Func<string, List<string>, bool>> ContentValidationMap { get; set; }
    }
}
