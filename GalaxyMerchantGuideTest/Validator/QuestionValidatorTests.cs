﻿using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuide.MessageValidator;
using GalaxyMerchantGuideTest.Data;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Validator
{
    public class QuestionValidatorTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            // Prepare Data
            var testData = new TestMessageData(_mockMessageInfoBase);
            _mockMessageInfoBase.CurrencyMap.Returns(testData.CurrencyMap);
            _mockMessageInfoBase.GalaticCurrencyMap.Returns(testData.GalaticCurrencyMap);
            _mockMessageInfoBase.TradeMetalValueMap.Returns(testData.TradeMetalValueMap);
            _mockMessageInfoBase.InputValidationMap.Returns(testData.InputValidationMap);
            _mockMessageInfoBase.ContentValidationMap.Returns(testData.ContentValidationMap);
        }

        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Input Processor
        /// </summary>
        private readonly IInputMessage _mockInputProcessor = Substitute.For<IInputMessage>();

        /// <summary>
        /// Question Validator Input and Content Validators Count Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_NumberOfValidations_Test()
        {
            // Question Validation Objects
            var questionValidator = new QuestionMessageValidator(_mockMessageInfoBase,_mockInputProcessor);

            // Asserts
            Assert.AreEqual(questionValidator.InputValidators.Count,1); // Only 1 Input Validator
            Assert.AreEqual(questionValidator.ContentValidators.Count, 4); // 4 Content Validator
        }

        /// <summary>
        /// Question Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_CheckValidInput_Test()
        {
            // Arrange Data
            var testData = "how many Credits is glob pish Iron ?";
            _mockInputProcessor.ActualMessage = testData;

            // Question Validation Objects
            var questionValidator = new QuestionMessageValidator(_mockMessageInfoBase, _mockInputProcessor);
            var isInputValid =
                questionValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, true); // Input Validation Assertion
        }

        /// <summary>
        /// Question Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_CheckInValidInput_Test()
        {
            // Arrange Data
            var testData = "how many Credits is glob pish mock Iron";
            _mockInputProcessor.ActualMessage = testData;

            // Question Validation Objects
            var questionValidator = new QuestionMessageValidator(_mockMessageInfoBase, _mockInputProcessor);
            var isInputValid =
                questionValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, false); // Input Validation Assertion
        }

        // ToDo: To Implement the Question Content Validation use cases, a design change is required, currently a Total Trade Value is only in the 
        // ToDo: Question class not in the base interface IInput, since Assignment and Derivation doesn't need it, but it fails while typecasting the mocking
        // ToDo: Framework object, either need to find a workaround or make the tradevalue generic across IInput types
        /////// <summary>
        /////// Question Valid Content Test.
        /////// </summary>
        /////// <returns></returns>
        ////[Test]
        ////public void Question_CheckValidContent_Test()
        ////{
        ////    // Arrange Data
        ////    var testData = "how many Credits is glob pish Iron ?";
        ////    _inputProcessor.ActualMessage = testData;
        ////    var mockActualMessageList = _inputProcessor.ActualMessage.ConvertToList();
        ////    var processedMessageList = _inputProcessor.ActualMessage.ConvertToList(ConstantData.QuestionIgnoreData);
        ////    var processedMessage = processedMessageList.ConvertToMessage();
           

        ////    // Assignemnt Validation Objects
        ////    var questionValidator = new QuestionValidator(_inputProcessor);

        ////    var isContentValid =
        ////        questionValidator.IsContentValid(processedMessage, processedMessageList);

        ////    // Asserts
        ////    Assert.AreEqual(isContentValid, true); // Input Validation Assertion
        ////}

        /////// <summary>
        /////// Question Valid Content Test.
        /////// </summary>
        /////// <returns></returns>
        ////[Test]
        ////public void Question_CheckInValidContent_Test()
        ////{
        ////    // Arrange Data
        ////    var testData = "how many Credits is glob pish mock Iron ?";
        ////    _inputProcessor.ActualMessage = testData;
        ////    var mockActualMessageList = _inputProcessor.ActualMessage.ConvertToList();
        ////    var processedMessageList = _inputProcessor.ActualMessage.ConvertToList(ConstantData.QuestionIgnoreData);
        ////    var processedMessage = processedMessageList.ConvertToMessage();


        ////    // Assignemnt Validation Objects
        ////    var questionValidator = new QuestionValidator(_inputProcessor);

        ////    var isContentValid =
        ////        questionValidator.IsContentValid(processedMessage, processedMessageList);

        ////    // Asserts
        ////    Assert.AreEqual(isContentValid, false); // Input Validation Assertion
        ////}


        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockInputProcessor.ClearSubstitute();
        }
    }
}
