﻿using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuide.MessageValidator;
using GalaxyMerchantGuideTest.Data;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Validator
{
    public class AssignmentValidatorTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            // Prepare Data
            var testData = new TestMessageData(_mockMessageInfoBase);
            _mockMessageInfoBase.CurrencyMap.Returns(testData.CurrencyMap);
            _mockMessageInfoBase.GalaticCurrencyMap.Returns(testData.GalaticCurrencyMap);
            _mockMessageInfoBase.TradeMetalValueMap.Returns(testData.TradeMetalValueMap);
            _mockMessageInfoBase.InputValidationMap.Returns(testData.InputValidationMap);
            _mockMessageInfoBase.ContentValidationMap.Returns(testData.ContentValidationMap);
        }

        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Input Processor
        /// </summary>
        private readonly IInputMessage _mockInputProcessor = Substitute.For<IInputMessage>();

        /// <summary>
        /// Assignment Validator Input and Content Validators Count Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_NumberOfValidations_Test()
        {
            // Assignemnt Validation Objects
            var assignmentValidator = new AssignmentMessageValidator(_mockMessageInfoBase,_mockInputProcessor);

            // Asserts
            Assert.AreEqual(assignmentValidator.InputValidators.Count,1); // Only 1 Input Validator
            Assert.AreEqual(assignmentValidator.ContentValidators.Count, 0); // 0 Content Validator
        }

        /// <summary>
        /// Assignment Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_CheckValidInput_Test()
        {
            // Arrange Data
            var testData = "glob is I";
            _mockInputProcessor.ActualMessage = testData;

            // Assignemnt Validation Objects
            var assignmentValidator = new AssignmentMessageValidator(_mockMessageInfoBase,_mockInputProcessor);
            var isInputValid =
                assignmentValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, true); // Input Validation Assertion
        }

        /// <summary>
        /// Assignment Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_CheckInValidInput_Test()
        {
            // Arrange Data
            var testData = "glob is blob I";
            _mockInputProcessor.ActualMessage = testData;

            // Assignemnt Validation Objects
            var assignmentValidator = new AssignmentMessageValidator(_mockMessageInfoBase,_mockInputProcessor);
            var isInputValid =
                assignmentValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, false); // Invalid Input Assertion
        }

        /// <summary>
        /// Assignment Valid Content Test. Always return true, since there are no explicit content tests for the Assignment Type
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_ContentValidation_Test()
        {
            // Arrange Data
            var testData = "glob is I";
            _mockInputProcessor.ActualMessage = testData;

            // Assignemnt Validation Objects
            var assignmentValidator = new AssignmentMessageValidator(_mockMessageInfoBase,_mockInputProcessor);
            var isInputValid =
                assignmentValidator.IsContentValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, true); // Input Validation Assertion
        }


        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockInputProcessor.ClearSubstitute();
        }
    }
}
