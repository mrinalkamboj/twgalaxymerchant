﻿using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuide.MessageValidator;
using GalaxyMerchantGuideTest.Data;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Validator
{
    public class DerivationValidatorTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            // Prepare Data
            var testData = new TestMessageData(_mockMessageInfoBase);
            _mockMessageInfoBase.CurrencyMap.Returns(testData.CurrencyMap);
            _mockMessageInfoBase.GalaticCurrencyMap.Returns(testData.GalaticCurrencyMap);
            _mockMessageInfoBase.TradeMetalValueMap.Returns(testData.TradeMetalValueMap);
            _mockMessageInfoBase.InputValidationMap.Returns(testData.InputValidationMap);
            _mockMessageInfoBase.ContentValidationMap.Returns(testData.ContentValidationMap);
        }


        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Input Processor
        /// </summary>
        private readonly IInputMessage _mockInputProcessor = Substitute.For<IInputMessage>();

        /// <summary>
        /// Derivation Validator Input and Content Validators Count Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_NumberOfValidations_Test()
        {
            // Derivation Validation Objects
            var derivationValidator = new DerivationMessageValidator(_mockMessageInfoBase,_mockInputProcessor);

            // Asserts
            Assert.AreEqual(derivationValidator.InputValidators.Count,1); // Only 1 Input Validator
            Assert.AreEqual(derivationValidator.ContentValidators.Count, 4); // 4 Content Validator
        }

        /// <summary>
        /// Derivation Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_CheckValidInput_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 Credits";
            _mockInputProcessor.ActualMessage = testData;

            // Derivation Validation Objects
            var derivationValidator = new DerivationMessageValidator(_mockMessageInfoBase, _mockInputProcessor);
            var isInputValid =
                derivationValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, true); // Input Validation Assertion
        }

        /// <summary>
        /// Derivation Valid Input Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_CheckInValidInput_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 No Credits";
            _mockInputProcessor.ActualMessage = testData;

            // Derivation Validation Objects
            var derivationValidator = new DerivationMessageValidator(_mockMessageInfoBase, _mockInputProcessor);
            var isInputValid =
                derivationValidator.IsInputValid(_mockInputProcessor.ActualMessage, _mockInputProcessor.ActualMessageList);

            // Asserts
            Assert.AreEqual(isInputValid, false); // Input Validation Assertion
        }

        /// <summary>
        /// Derivation Valid Content Test.
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_CheckValidContent_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 Credits";
            _mockInputProcessor.ActualMessage = testData;
            var processedMessageList = _mockInputProcessor.ActualMessage.ConvertToList(ConstantData.DerivationIgnoreData);
            var processedMessage = processedMessageList.ConvertToMessage();
           

            // Assignemnt Validation Objects
            var derivationValidator = new DerivationMessageValidator(_mockMessageInfoBase, _mockInputProcessor);

            var isContentValid =
                derivationValidator.IsContentValid(processedMessage, processedMessageList);

            // Asserts
            Assert.AreEqual(isContentValid, true); // Input Validation Assertion
        }

        /// <summary>
        /// Derivation Valid Content Test.
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_CheckInValidContent_Test()
        {
            // Arrange Data
            var testData = "glob prok mock Gold is 57800 Credits";
            _mockInputProcessor.ActualMessage = testData;
            var processedMessageList = _mockInputProcessor.ActualMessage.ConvertToList(ConstantData.DerivationIgnoreData);
            var processedMessage = processedMessageList.ConvertToMessage();


            // Derivation Validation Objects
            var derivationValidator = new DerivationMessageValidator(_mockMessageInfoBase, _mockInputProcessor);

            var isContentValid =
                derivationValidator.IsContentValid(processedMessage, processedMessageList);

            // Asserts
            Assert.AreEqual(isContentValid, false); // Input Validation Assertion
        }


        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockInputProcessor.ClearSubstitute();
        }
    }
}
