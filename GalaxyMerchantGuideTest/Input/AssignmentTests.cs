﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaxyMerchantGuide;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuideTest.Data;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Input
{
    public class AssignmentTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            // Prepare Data
            var testData = new TestMessageData(_mockMessageInfoBase);
            _mockMessageInfoBase.CurrencyMap.Returns(testData.CurrencyMap);
            _mockMessageInfoBase.GalaticCurrencyMap.Returns(testData.GalaticCurrencyMap);
            _mockMessageInfoBase.TradeMetalValueMap.Returns(testData.TradeMetalValueMap);
            _mockMessageInfoBase.InputValidationMap.Returns(testData.InputValidationMap);
            _mockMessageInfoBase.ContentValidationMap.Returns(testData.ContentValidationMap);
        }

        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Validation Processor
        /// </summary>
        private readonly Func<string,MessageInfoBase, IInputMessage, IMessageValidator> _mockvalidationProcessor = Substitute.For<Func<string, MessageInfoBase, IInputMessage, IMessageValidator>>();
         

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_ActualAndProcessedMessage_Test()
        {
            // Arrange Data
            var testData = "glob is I" ;

            var assignment = new Assignment(testData, _mockMessageInfoBase, _mockvalidationProcessor);

            // Asserts
            Assert.AreEqual(assignment.ActualMessage,testData);
            Assert.AreEqual(assignment.ActualMessageList.Count,3);
            Assert.AreEqual(assignment.ProcessedMessage,"glob I");
            Assert.AreEqual(assignment.ProcessedMessageList.Count,2);
        }


        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_ValidationCheckMethod_Test()
        {
            // Arrange Data
            var testData = "glob is I";

            // Assignment Class object
            var assignment = new Assignment(testData, _mockMessageInfoBase, _mockvalidationProcessor);

            // Mock IsInputValid call to always return true
            _mockvalidationProcessor(ConstantData.Assignment, _mockMessageInfoBase, assignment)
                .IsInputValid(assignment.ActualMessage, assignment.ActualMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnValue = assignment.ValidationCheck();

            // Asserts
            Assert.AreEqual(true,returnValue);
           
        }

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Assignment_ProcessMethod_Test()
        {
            // Arrange Data
            var fixedTestData = "glob is I";

            // Assignment Class object
            var assignment = new Assignment(fixedTestData, _mockMessageInfoBase, _mockvalidationProcessor);

            // Mock IsInputValid call to always return true
            _mockvalidationProcessor(ConstantData.Assignment, _mockMessageInfoBase, assignment)
                .IsContentValid(assignment.ProcessedMessage, assignment.ProcessedMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnInputValue = assignment.Process();

            // Asserts
            Assert.AreEqual(true, returnInputValue.IsValid);
        }

        

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockvalidationProcessor.ClearSubstitute();
        }
    }
}
