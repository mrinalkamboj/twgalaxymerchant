﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaxyMerchantGuide;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;
using GalaxyMerchantGuide.MessageInfo;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Input
{
    public class QuestionTests
    {
        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Validation Processor
        /// </summary>
        private readonly Func<string, MessageInfoBase, IInputMessage, IMessageValidator> _mockValidationProcessor = Substitute.For<Func<string, MessageInfoBase, IInputMessage, IMessageValidator>>();

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_ActualAndProcessedMessage_Test()
        {
            // Arrange Data
            var testData = "how many Credits is tegj pish Silver ?";

            // Question Class object
            var question = new Question(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Asserts
            Assert.AreEqual(question.ActualMessage,testData);
            Assert.AreEqual(question.ActualMessageList.Count,8);
            Assert.AreEqual(question.ProcessedMessage, "tegj pish Silver");
            Assert.AreEqual(question.ProcessedMessageList.Count,3);
        }


        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_ValidationCheckMethod_Test()
        {
            // Arrange Data
            var testData = "how many Credits is tegj pish Silver ?";

            // Derivation Class object
            var question = new Question(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Mock IsInputValid call to always return true
            _mockValidationProcessor(ConstantData.Question, _mockMessageInfoBase, question)
                .IsInputValid(question.ActualMessage, question.ActualMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnValue = question.ValidationCheck();

            // Asserts
            Assert.AreEqual(true, returnValue);  
        }

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Question_ProcessMethod_Test()
        {
            // Arrange Data
            var testData = "how many Credits is tegj pish Silver ?";

            // Derivation Class object
            var question = new Question(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Mock IsInputValid call to always return true
            _mockValidationProcessor(ConstantData.Question, _mockMessageInfoBase, question)
                .IsContentValid(question.ProcessedMessage, question.ProcessedMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnInputValue = question.Process();

            // Asserts
            Assert.AreEqual(true, returnInputValue.IsValid);
        }

        

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockValidationProcessor.ClearSubstitute();
        }
    }
}
