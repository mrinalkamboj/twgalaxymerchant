﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaxyMerchantGuide;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.Message;
using GalaxyMerchantGuide.MessageInfo;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Input
{
    public class DerivationTests
    {
        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Mock For Validation Processor
        /// </summary>
        private readonly Func<string, MessageInfoBase, IInputMessage, IMessageValidator> _mockValidationProcessor = Substitute.For<Func<string, MessageInfoBase, IInputMessage, IMessageValidator>>();

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_ActualAndProcessedMessage_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 Credits";

            // Derivation Class object
            var derivation = new Derivation(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Asserts
            Assert.AreEqual(derivation.ActualMessage,testData);
            Assert.AreEqual(derivation.ActualMessageList.Count,6);
            Assert.AreEqual(derivation.ProcessedMessage, "glob prok Gold 57800");
            Assert.AreEqual(derivation.ProcessedMessageList.Count,4);
        }


        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_ValidationCheckMethod_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 Credits";

            // Derivation Class object
            var derivation = new Derivation(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Mock IsInputValid call to always return true
            _mockValidationProcessor(ConstantData.Derivation, _mockMessageInfoBase, derivation)
                .IsInputValid(derivation.ActualMessage, derivation.ActualMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnValue = derivation.ValidationCheck();

            // Asserts
            Assert.AreEqual(true,returnValue);  
        }

        /// <summary>
        /// Assignment Actual and Processed Message Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public void Derivation_ProcessMethod_Test()
        {
            // Arrange Data
            var testData = "glob prok Gold is 57800 Credits";

            // Derivation Class object
            var derivation = new Derivation(testData, _mockMessageInfoBase, _mockValidationProcessor);

            // Mock IsInputValid call to always return true
            _mockValidationProcessor(ConstantData.Derivation, _mockMessageInfoBase, derivation)
                .IsContentValid(derivation.ProcessedMessage, derivation.ProcessedMessageList).ReturnsForAnyArgs(true);

            // Actual Method Call
            var returnInputValue = derivation.Process();

            // Asserts
            Assert.AreEqual(true, returnInputValue.IsValid);
        }

        

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _mockValidationProcessor.ClearSubstitute();
        }
    }
}
