﻿using GalaxyMerchantGuide.ContentInfo;
using GalaxyMerchantGuide.Extension;
using GalaxyMerchantGuide.MessageInfo;
using GalaxyMerchantGuideTest.Data;
using NSubstitute;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Other
{
    public class RomanNumeralTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            // Prepare Data
            var testData = new TestMessageData(_mockMessageInfoBase);
            _mockMessageInfoBase.CurrencyMap.Returns(testData.CurrencyMap);
            _mockMessageInfoBase.GalaticCurrencyMap.Returns(testData.GalaticCurrencyMap);
            _mockMessageInfoBase.TradeMetalValueMap.Returns(testData.TradeMetalValueMap);
            _mockMessageInfoBase.InputValidationMap.Returns(testData.InputValidationMap);
            _mockMessageInfoBase.ContentValidationMap.Returns(testData.ContentValidationMap);
        }

        /// <summary>
        /// Mock Message Info Base
        /// </summary>
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Roman Numeral Data Validity Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_IsValid_Test()
        {
            // Arrange Data
            var fixedTestData = "I V X L C D M";
            var testList = fixedTestData.ConvertToList(); // Create List

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidRomanExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(fixedTestData, testList);

            // Assert
            Assert.IsTrue(isValid);
        }

        /// <summary>
        /// Roman Numeral Data Validity Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_IsInValid_Test()
        {
            // Arrange Data
            var fixedTestData = "I J V X L C D M";
            var testList = fixedTestData.ConvertToList(); // Create List

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidRomanExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(fixedTestData, testList);

            // Assert
            Assert.IsFalse(isValid);
        }

        /// <summary>
        /// Roman Numeral DLV repeat pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_DLVRepeatPattern_Test()
        {
            // Arrange Data
            var testData = "I V X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidDlvPatternExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsTrue(isValid);
        }

        /// <summary>
        /// Roman Numeral DLV repeat pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_InvalidDLVRepeatPattern_Test()
        {
            // Arrange Data
            var testData = "I V D X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidDlvPatternExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsFalse(isValid);
        }

        /// <summary>
        /// Roman Numeral ICXM repeat pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_ICXMRepeatPattern_Test()
        {
            // Arrange Data
            var testData = "I V X X X I X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidIxcmRepeatExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsTrue(isValid);
        }

        /// <summary>
        /// Roman Numeral ICXM repeat pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_InvalidICXMRepeatPattern_Test()
        {
            // Arrange Data
            var testData = "I V X X X X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidIxcmRepeatExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsFalse(isValid);
        }

        /// <summary>
        /// Roman Numeral Subtraction pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_ValidSubtractionPattern_Test()
        {
            // Arrange Data
            var testData = "I V X X X I X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidSubtractionPatternExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsTrue(isValid);
        }

        /// <summary>
        /// Roman Numeral Subtraction pattern test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_InValidSubtractionPattern_Test()
        {
            // Arrange Data
            var testData = "I C X X X I X L C D M";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralDelegate = ContentInfoData.CheckOnlyValidSubtractionPatternExist(_mockMessageInfoBase);

            // Call Delegate with relevant Parameters
            var isValid = romanNumeralDelegate(testData, testList);

            // Assert
            Assert.IsFalse(isValid);
        }

        /// <summary>
        /// Roman Numeral Value test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_FetchRomanValue_Test_1()
        {
            // Arrange Data
            var testData = "M M V I";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralValue = testList.FetchRomanValue(_mockMessageInfoBase);

            // Assert
            Assert.AreEqual(romanNumeralValue,2006);
        }

        /// <summary>
        /// Roman Numeral Value test
        /// </summary>
        /// <returns></returns>
        [Test]
        public void RomanNumeral_FetchRomanValue_Test_2()
        {
            // Arrange Data
            var testData = "M C M X L I V";
            var testList = testData.ConvertToList(); // Create List

            //// Prepare Data
            //_mockMessageInfoBase.CurrencyMap.Returns(TestData.CurrencyMap);
            //_mockMessageInfoBase.GalaticCurrencyMap.Returns(TestData.GalaticCurrencyMap);

            // Func Delegate for Logical Processing
            var romanNumeralValue = testList.FetchRomanValue(_mockMessageInfoBase);

            // Assert
            Assert.AreEqual(romanNumeralValue, 1944);
        }
    }
}
