﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaxyMerchantGuide;
using GalaxyMerchantGuide.Constants;
using GalaxyMerchantGuide.Enum;
using GalaxyMerchantGuide.Interface;
using GalaxyMerchantGuide.MessageInfo;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;

namespace GalaxyMerchantGuideTest.Main
{
    public class TradeTests
    {
         /// <summary>
         /// Mock / Sustitute Classes
         /// </summary>
         private readonly IInputReader _readerMock = Substitute.For<IInputReader>();
        private readonly MessageInfoBase _mockMessageInfoBase = Substitute.For<MessageInfoBase>();
        private readonly Func<string, string, MessageInfoBase, IInputMessage> _inputProcessorMock = Substitute.For<Func<string, string, MessageInfoBase, IInputMessage>>();
         private readonly IOutputWriter _writerMock = Substitute.For<IOutputWriter>();
         private readonly MessageInfoBase _messageBaseMock = Substitute.For<MessageInfoBase>();

        /// <summary>
        /// Read Input Unit Tests
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Trade_ReadInput_Test()
        {
            // Arrange Data
            var testData = new List<string> { "glob is I" };

            // Prepare Returns
            _readerMock.Read(InputReaderType.FileCurrency).Returns(Task.FromResult(testData));
            _readerMock.Read(InputReaderType.FileInput).Returns(Task.FromResult(testData));

            // Actual method call wit mock arguments
            var trade = new Trade(_readerMock, _messageBaseMock, _inputProcessorMock, _writerMock);
            await trade.ReadInput();

            // Asserts
            Assert.IsTrue(_readerMock.ReceivedCalls().Any()); // Reader Mock received a Call
            Assert.IsNotNull(trade.InputDataList); // Collection is Not null (It is Initialized)
            Assert.IsNotNull(trade.InputMessageList); // Collection is Not Null (It is Initialized)
            Assert.AreEqual(testData.First(), trade.InputDataList.First()); // Compare Values
        }

        /// <summary>
        /// Trade Assignment Type Unit Test 
        /// </summary>
        [Test]
        public void Trade_BeginProcessing_Assignment_Test()
        {
            // Arrange Data
            var testData = "glob is I";

            // Trade Object with Mock Arguments
            var trade = new Trade(_readerMock, _messageBaseMock, _inputProcessorMock, _writerMock)
            {
                InputDataList = new List<string> {testData}
            };

            // Create Mock Object 
            var assignmentMock = _inputProcessorMock(ConstantData.Assignment, testData, _messageBaseMock);

            // Return Value from a Mock Method Call
            assignmentMock.ValidationCheck().Returns(true);

            // Actual Code Call
            trade.BeginProcessing();

            Assert.IsTrue(_inputProcessorMock.ReceivedCalls().Any()); // InputProcessor Mock received a Call
            Assert.IsNotNull(trade.InputMessageList); // InputMessageList is Not Null
            Assert.IsTrue(trade.InputMessageList.Count > 0);
            Assert.IsNotNull(trade.InputDataList); // InputDataList is not null

        }


        /// <summary>
        /// Trade Derivation Type Unit Test 
        /// </summary>
        [Test]
        public void Trade_BeginProcessing_Derivation_Test()
        {
            // Arrange Data
            var testData = "glob glob Silver is 34 Credits";

            // Trade Object with Mock Arguments
            var trade = new Trade(_readerMock, _messageBaseMock, _inputProcessorMock, _writerMock)
            {
                InputDataList = new List<string> { testData }
            };

            // Create Mock Object 
            var derivationMock = _inputProcessorMock(ConstantData.Derivation, testData, _messageBaseMock);

            // Return Value from a Mock Method Call
            derivationMock.ValidationCheck().Returns(true);

            // Actual Code Call
            trade.BeginProcessing();

            Assert.IsTrue(_inputProcessorMock.ReceivedCalls().Any()); // InputProcessor Mock received a Call
            Assert.IsNotNull(trade.InputMessageList); // InputMessageList is Not Null
            Assert.IsTrue(trade.InputMessageList.Count > 0);
            Assert.IsNotNull(trade.InputDataList); // InputDataList is not null
        }


        /// <summary>
        /// Trade Question Type Unit Test 
        /// </summary>
        [Test]
        public void Trade_BeginProcessing_Question_Test()
        {
            // Arrange Data
            var testData = "how many Credits is glob prok Gold ?";

            // Trade Object with Mock Arguments
            var trade = new Trade(_readerMock, _messageBaseMock, _inputProcessorMock, _writerMock)
            {
                InputDataList = new List<string> { testData }
            };

            // Create Mock Object 
            var questionMock = _inputProcessorMock(ConstantData.Derivation, testData, _messageBaseMock);

            // Return Value from a Mock Method Call
            questionMock.ValidationCheck().Returns(true);

            // Actual Code Call
            trade.BeginProcessing();

            Assert.IsTrue(_inputProcessorMock.ReceivedCalls().Any()); // InputProcessor Mock received a Call
            Assert.IsNotNull(trade.InputMessageList); // InputMessageList is Not Null
            Assert.IsTrue(trade.InputMessageList.Count > 0);
            Assert.IsNotNull(trade.InputDataList); // InputDataList is not null
        }


        /// <summary>
        /// Trade Write Output Type Unit Test 
        /// </summary>
        [Test]
        public void Trade_WriteOutput_Test()
        {
            // Actual method call wit mock arguments
            var trade = new Trade(_readerMock, _messageBaseMock, _inputProcessorMock, _writerMock);
            trade.WriteOutput();

            // Asserts
            Assert.IsTrue(_writerMock.ReceivedCalls().Any()); // Reader Mock received a Call
        }

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _readerMock.ClearSubstitute();
            _inputProcessorMock.ClearSubstitute();
            _writerMock.ClearSubstitute();
            _messageBaseMock.ClearSubstitute();
        }
    }
}
